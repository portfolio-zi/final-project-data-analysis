En esta carpeta se encuentran los notebooks del proyecto, así como los datos de entrada y todo lo generado por ellos.


## Data from:

https://www.kaggle.com/edumagalhaes/quality-prediction-in-a-mining-process

Context
It is not always easy to find databases from real world manufacturing plants, specially mining plants. So, I would like to share this database with the community, which comes from one of the most important parts of a mining process: a flotation plant!

PLEASE HELP ME GET MORE DATASETS LIKE THIS FILLING A 30s SURVEY:

The main goal is to use this data to predict how much impurity is in the ore concentrate. As this impurity is measured every hour, if we can predict how much silica (impurity) is in the ore concentrate, we can help the engineers, giving them early information to take actions (empowering!). Hence, they will be able to take corrective actions in advance (reduce impurity, if it is the case) and also help the environment (reducing the amount of ore that goes to tailings as you reduce silica in the ore concentrate).

Content
The first column shows time and date range (from march of 2017 until september of 2017). Some columns were sampled every 20 second. Others were sampled on a hourly base.

The second and third columns are quality measures of the iron ore pulp right before it is fed into the flotation plant. Column 4 until column 8 are the most important variables that impact in the ore quality in the end of the process. From column 9 until column 22, we can see process data (level and air flow inside the flotation columns, which also impact in ore quality. The last two columns are the final iron ore pulp quality measurement from the lab.
Target is to predict the last column, which is the % of silica in the iron ore concentrate.

Inspiration
I have been working in this dataset for at least six months and would like to see if the community can help to answer the following questions:

Is it possible to predict % Silica Concentrate every minute?

How many steps (hours) ahead can we predict % Silica in Concentrate? This would help engineers to act in predictive and optimized way, mitigatin the % of iron that could have gone to tailings.

Is it possible to predict % Silica in Concentrate whitout using % Iron Concentrate column (as they are highly correlated)?


Summary
This dataset is about a flotation plant which is a process used to concentrate the iron ore. This process is very common in a mining plant.

Goal of this dataset
The target is to predict the % of Silica in the end of the process, which is the concentrate of iron ore and its impurity (which is the % of Silica).

Why prediction is needed
Although the % of Silica is measured (last column), its a lab measurement, which means that it takes at least one hour for the process engineers to have this value. So if it is posible to predict the amount of impurity in the process

Columns
Date and Timestampe
% Iron Feed % of Iron that comes from the iron ore that is being fed into the flotation cells
% Silica Feed % of silica (impurity) that comes from the iron ore that is being fed into the flotation cells
Starch Flow Starch (reagent) Flow measured in m3/h
Amina Flow Amina (reagent) Flow measured in m3/h
Ore Pulp Flow t/h
Ore Pulp pH pH scale from 0 to 14
Ore Pulp Density Density scale from 1 to 3 kg/cm³
Flotation Column 01 Air Flow Air flow that goes into the flotation cell measured in Nm³/h
Flotation Column 02 Air Flow Air flow that goes into the flotation cell measured in Nm³/h
Flotation Column 03 Air Flow Air flow that goes into the flotation cell measured in Nm³/h
Flotation Column 04 Air Flow Air flow that goes into the flotation cell measured in Nm³/h
Flotation Column 05 Air Flow Air flow that goes into the flotation cell measured in Nm³/h
Flotation Column 06 Air Flow Air flow that goes into the flotation cell measured in Nm³/h
Flotation Column 07 Air Flow Air flow that goes into the flotation cell measured in Nm³/h
Flotation Column 01 Level Froth level in the flotation cell measured in mm (millimeters)
Flotation Column 02 Level Froth level in the flotation cell measured in mm (millimeters)
Flotation Column 03 Level Froth level in the flotation cell measured in mm (millimeters)
Flotation Column 04 Level Froth level in the flotation cell measured in mm (millimeters)
Flotation Column 05 Level Froth level in the flotation cell measured in mm (millimeters)
Flotation Column 06 Level Froth level in the flotation cell measured in mm (millimeters)
Flotation Column 07 Level Froth level in the flotation cell measured in mm (millimeters)
% Iron Concentrate % of Iron which represents how much iron is presented in the end of the flotation process (0-100%, lab measurement)
% Silica Concentrate % of silica which represents how much iron is presented in the end of the flotation process (0-100%, lab measurement)
