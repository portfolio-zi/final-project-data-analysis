notebooks-zip:
	git archive HEAD -o notebooks.tar
	tar -f notebooks.tar --delete dashboard
	tar -f notebooks.tar --delete .gitignore
	tar -f notebooks.tar --delete ABM_SoftSensor_MachineLearning_DeepLearning.pdf
	tar -f notebooks.tar --delete 'CRISP-DM Analysis Template.ipynb'
	tar -f notebooks.tar --delete CRISP-DM_dataset4.ipynb 
	tar -f notebooks.tar --delete Makefile
	gzip < notebooks.tar > notebooks.tgz

dashboard-zip:
	git archive -o dashboard.tgz HEAD:dashboard
