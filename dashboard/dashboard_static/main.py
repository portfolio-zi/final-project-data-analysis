# Pandas for data management
import pandas as pd
try:
    import dask.dataframe as dd
except:
    pass


# Bokeh basics
from bokeh.io import curdoc
from bokeh.models.widgets import Tabs


# Each tab is drawn by one script
from scripts.raw_material import raw_material_tab
from scripts.operation import operations_tab
from scripts.quality import quality_tab


# Using included state data from Bokeh for map
from bokeh.sampledata.us_states import data as states

# Read data into dataframes
data = pd.read_pickle('dashboard_static/data/flotation_plant.pkl')

# No se consigure plotear con dask
#data = dd.from_pandas(data, npartitions=8)
try:
    data = dd.from_pandas(data)
except:
    pass

# Create each of the tabs
tab_raw = raw_material_tab(data)
tab_op = operations_tab(data)
tab_qua = quality_tab(data)

# Put all the tabs into one application
tabs = Tabs(tabs=[tab_raw, tab_op, tab_qua])

# Put the tabs in the current document for display
curdoc().add_root(tabs)
