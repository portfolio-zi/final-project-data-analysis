# pandas and numpy for data manipulation
import pandas as pd
import numpy as np

from scripts.plots import *

from bokeh.models import Panel
from bokeh.models.widgets import Tabs
from bokeh.layouts import column, row, WidgetBox


def raw_material_tab(data):

    iron_p = make_timeseries_plot(
        data, 'datetime', 'iron_p', title='Incomming Iron (%)', hover_labels=['date', 'Iron (%)'])
    silica_p = make_timeseries_plot(
        data, 'datetime', 'silica_p', title='Incomming Silica (%)', hover_labels=['date', 'Silica (%)'])

    # Create a row layout
    layout = column(iron_p, silica_p)

    # Make a tab with the layout
    tab = Panel(child=layout, title='Raw Material Data')

    return tab
