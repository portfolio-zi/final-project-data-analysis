# pandas and numpy for data manipulation
import pandas as pd
import numpy as np

from scripts.plots import *

from bokeh.models import Panel, Paragraph, Div
from bokeh.models.widgets import Tabs
from bokeh.layouts import column, row, WidgetBox


def operations_tab(data):

    flow_plots = []
    level_plots = []
#    for i in range(1, 7):
    for i in range(1, 2):

        flow_p = make_timeseries_plot(
            data, 'datetime', f'c{i}_air_f', title=f'Column {i} air flow', hover_labels=['date', 'Air flow'])
        level_p = make_timeseries_plot(
            data, 'datetime', f'c{i}_level', title=f'Column {i} level', hover_labels=['date', 'Level'])
        flow_plots.append(flow_p)
        level_plots.append(level_p)

    # Create a row layout
    layout = column()
#    for i in range(0, 6):
    for i in range(0, 1):
        p = Div(text=f"<h2>Column {i+1}</h2>", width=200, height=100)
        layout.children.append(column(p, row(flow_plots[i], level_plots[i])))

    # Make a tab with the layout
    tab = Panel(child=layout, title='Operation Material Data')

    return tab
