# pandas and numpy for data manipulation
import pandas as pd
import numpy as np

from scripts.plots import *

from bokeh.models import Panel
from bokeh.models.widgets import Tabs
from bokeh.layouts import column, row, WidgetBox


def quality_tab(data):

    iron_p = make_timeseries_plot(
        data, 'datetime', 'final_iron_p', title='Outcomming Iron (%)', hover_labels=['date', 'Final Iron (%)'])
    silica_p = make_timeseries_plot(
        data, 'datetime', 'final_silica_p', title='Outcomming Silica (%)', hover_labels=['date', 'Final Silica (%)'])

    # Create a row layout
    layout = column(iron_p, silica_p)

    # Make a tab with the layout
    tab = Panel(child=layout, title='Final Quality Data')

    return tab
