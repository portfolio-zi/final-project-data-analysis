# numpy for data manipulation
import numpy as np


from bokeh.plotting import figure
from bokeh.models import ColumnDataSource, DatetimeTickFormatter
from bokeh.models.tools import HoverTool

def make_timeseries_plot(data, x, y, title, hover_labels):
	
    source = ColumnDataSource(data)
    
    
    TOOLS = 'save,pan,box_zoom,reset,wheel_zoom,hover'
    p = figure(title=title, x_axis_type='datetime', y_axis_type="linear",
               plot_height = 300, tools = TOOLS, plot_width = 800)
    p.xaxis.axis_label = hover_labels[0]
    p.yaxis.axis_label = hover_labels[1]

    hover = p.select(dict(type=HoverTool))
    hover.tooltips = [('date', '@date{%F %T}'),
                      ('Iron (%)', '@iron_p')
                      ]
    p.xaxis[0].formatter = DatetimeTickFormatter(
        seconds="%F %H",
        minutes="%F %H",
        hours="%F %H",
        days="%F %H",
        months="%F %H",
        years="%F %H",
    )
    p.xaxis.major_label_orientation = np.pi/4
    hover.formatters = {'@date': 'datetime'}
    p.line(x, y, source=source, line_color="blue", line_width = 3)

    return p
