# Dashboard para visualizar los datos.

Este dashboard basando en bokeh puede usarse para visulaizar los datos.

## Para arrancar
Desde la línea de comandos lanzar el script `start_dashborad.py`

El programa no está preparado para correr en Windows.

## Nota
En el portátil desarrollado no se pueden cargar todos los datos (memoria insuficiente).
He intentado utilizar `dask` para cargar los `dataframe` fuera de memoria, pero de momento ha sido en vano. 

Si se quieren los datos de todas las columnas de flotación; hay que cambiar cunatas columnas se cargan. Para ello modificar (comentar/descomentar) las líneas 16 y 28 en `dashboard_static/scripts/operation.py`. 
